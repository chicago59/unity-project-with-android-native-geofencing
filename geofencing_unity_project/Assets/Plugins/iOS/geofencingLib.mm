#import <Foundation/Foundation.h>

    #import <CoreLocation/CoreLocation.h>

    #import <UserNotifications/UserNotifications.h>

    typedef void( * MonoPMessageDelegate)(const char * message);

static MonoPMessageDelegate _messageDelegate = NULL;

static bool isNotificationPermissionGranted = false;

void askPermissionForNotification() {

    UNUserNotificationCenter * center = [UNUserNotificationCenter currentNotificationCenter];

    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;

    [center requestAuthorizationWithOptions: options completionHandler: ^ (BOOL granted, NSError * _Nullable error) {

        isNotificationPermissionGranted = granted;

    }];

}

FOUNDATION_EXPORT void RegisterCallbackHandler(MonoPMessageDelegate delegate)

{

    askPermissionForNotification();

    _messageDelegate = delegate;

}

void SendMessageToUnity(const char * message) {

    dispatch_async(dispatch_get_main_queue(), ^ {

        if (_messageDelegate != NULL) {

            _messageDelegate(message);

        }

    });

}

static CLLocationManager * locationManager = NULL;

static NSMutableArray < CLCircularRegion * > * regions = [
    [NSMutableArray alloc] init
];

static bool isRunning = false;

void createLocalNotification(NSString * title, NSString * message, UNNotificationTrigger * trigger, NSString * notificationId) {

    if (isNotificationPermissionGranted) {

        UNUserNotificationCenter * center = [UNUserNotificationCenter currentNotificationCenter];

        UNMutableNotificationContent * content = [
            [UNMutableNotificationContent alloc] init
        ];

        content.title = title;

        content.body = message;

        NSString * identifier = [@ "notification.id."
            stringByAppendingString: notificationId
        ];

        UNNotificationRequest * request = [UNNotificationRequest requestWithIdentifier: identifier content: content trigger: trigger];

        [center addNotificationRequest: request withCompletionHandler: nil];

    }

}

@interface LocationManagerDelegate: NSObject < CLLocationManagerDelegate >

    -(void) locationManager: (CLLocationManager * ) manager didEnterRegion: (CLRegion * ) region;

-
(void) locationManager: (CLLocationManager * ) manager didExitRegion: (CLRegion * ) region;

-
(void) locationManager: (CLLocationManager * ) manager didStartMonitoringForRegion: (CLRegion * ) region;

-
(void) locationManagerDidChangeAuthorization: (CLLocationManager * ) manager;

@end

@implementation LocationManagerDelegate

    -
    (void) locationManager: (CLLocationManager * ) manager didExitRegion: (CLRegion * ) region {

        NSString * resultMessage = [NSString stringWithFormat: @ "%s%@", "User exit region. Region name: ", region.identifier];

        const char * resultMesInChar = [resultMessage UTF8String];

        SendMessageToUnity(resultMesInChar);

        NSString * title = @ "User exit region";

        NSString * message = [NSString stringWithFormat: @ "%s%@", "Region name: ", region.identifier];

        NSString * identifier = region.identifier;

        UNTimeIntervalNotificationTrigger * trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval: 5 repeats: NO];

        createLocalNotification(title, message, trigger, identifier);

    }

    -
    (void) locationManager: (CLLocationManager * ) manager didEnterRegion: (CLRegion * ) region {

        NSString * resultMessage = [NSString stringWithFormat: @ "%s%@", "User enter region. Region name: ", region.identifier];

        const char * resultMesInChar = [resultMessage UTF8String];

        SendMessageToUnity(resultMesInChar);

        NSString * title = @ "User enter region";

        NSString * message = [NSString stringWithFormat: @ "%s%@", "Region name: ", region.identifier];

        NSString * identifier = region.identifier;

        UNTimeIntervalNotificationTrigger * trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval: 5 repeats: NO];

        createLocalNotification(title, message, trigger, identifier);

    }

    -
    (void) locationManager: (CLLocationManager * ) manager didStartMonitoringForRegion: (CLRegion * ) region {

        NSString * resultMessage = [NSString stringWithFormat: @ "%s%@", "didStartMonitoringForRegion Region name: ", region.identifier];

        const char * resultMesInChar = [resultMessage UTF8String];

        //        SendMessageToUnity(resultMesInChar);

    }

    -
    (void) locationManagerDidChangeAuthorization: (CLLocationManager * ) manager {

        if (@available(iOS 14.0, *)) {

            if (manager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {

                [locationManager requestAlwaysAuthorization];

            }

        } else {

            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {

                [locationManager requestAlwaysAuthorization];

            }

        }

    }

@end

static LocationManagerDelegate * locationManagerDelegate = [
    [LocationManagerDelegate alloc] init
];

FOUNDATION_EXPORT void AddRegion(float latitude, float longitude, float radius,
    const char * name) {

    CLLocationCoordinate2D regionCenter;

    regionCenter.latitude = latitude;

    regionCenter.longitude = longitude;

    NSString * idName = @(name);

    CLCircularRegion * region = [
        [CLCircularRegion alloc] initWithCenter: regionCenter radius: radius identifier: idName
    ];

    region.notifyOnEntry = true;

    region.notifyOnExit = true;

    [regions addObject: region];

    SendMessageToUnity("Region was added to the list");

}

FOUNDATION_EXPORT void RunIOSGeofencing() {

    locationManager = [
        [CLLocationManager alloc] init
    ];

    locationManager.delegate = locationManagerDelegate;

    [locationManager requestWhenInUseAuthorization];

    locationManager.allowsBackgroundLocationUpdates = true;

    locationManager.pausesLocationUpdatesAutomatically = false;

    for (int i = 0; i < [regions count]; i++)

    {

        CLCircularRegion * region = [regions objectAtIndex: i];

        NSString * resultMessage = [NSString stringWithFormat: @ "%s%@%s", "user triggered in ", region.identifier, " region"];

        NSString * title = @ "Region trigger event occur";

        UNLocationNotificationTrigger * notificationTrigger = [UNLocationNotificationTrigger triggerWithRegion: region repeats: NO];

        createLocalNotification(title, resultMessage, notificationTrigger, [@(i) stringValue]);

        [locationManager startMonitoringForRegion: region];

    }

    isRunning = true;

    SendMessageToUnity("Geofencing successfully started");

}

CLCircularRegion * FindRegion(NSString * name) {

    for (int i = 0; i < [regions count]; i++)

    {

        CLCircularRegion * region = [regions objectAtIndex: i];

        if (region.identifier == name) {

            return region;

        }

    }

    return NULL;

}

int FindRegionIndex(NSString * name) {

    int result = -1;

    for (int i = 0; i < [regions count]; i++)

    {

        CLCircularRegion * region = [regions objectAtIndex: i];

        if (region.identifier == name) {

            return i;

        }

    }

    return result;

}

FOUNDATION_EXPORT void RemoveRegion(const char * name) {

    int regionIndex = FindRegionIndex(@(name));

    CLCircularRegion * region = FindRegion(@(name));

    if (regionIndex == -1) return;

    if (isRunning) {

        [locationManager stopMonitoringForRegion: region];

    }

    //    SendMessageToUnity([[@(regionIndex) stringValue] UTF8String]);

    [regions removeObjectAtIndex: regionIndex];

    NSString * resultMessage = [NSString stringWithFormat: @ "%s%s%s", "Region with id: ", name, " successfully removed"];

    const char * resultMesInChar = [resultMessage UTF8String];

    //    SendMessageToUnity(resultMesInChar);

}