﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

    public static class Parser
    {
        public static char[] splitChars = new char[10] { '☀', '☭', '☢', '❤', '☂', '☯', '♞', '♫', '☎', '߷' };

        #region  Encrypt


        /// <summary>
        /// Set default numfirstSplitChar for main class parsing. 0 - for first attachment class, etc... 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="numfirstSplitChar"></param>
        /// <returns></returns>
        public static string Parse<T>(T value, int numfirstSplitChar = -1)
        {
            if (value == null)
            {
                if (typeof(T) == typeof(object))
                {
                    UnityEngine.Debug.Log(value + "| NNNNNNNNNNNNN OBJECT NULL NEED : TYPE " + "PARSER!!!");
                }

                return Parse(value, typeof(T), numfirstSplitChar);
            }
            else
            {
                return Parse(value, value.GetType(), numfirstSplitChar);
            }
        }

        public static string Parse<T>(T value, Type type, int num, string s = "")
        {

            if (type == typeof(int) || type == typeof(long) || type == typeof(float) || type == typeof(string) || type == typeof(TimeSpan) || type == typeof(uint) || type == typeof(ulong))
            {
                if (value == null)
                    return "null";
                return value.ToString();
            }
            else if (type == typeof(DateTime))
            {
                if (value == null)
                    return "null";
                return ((DateTime)(object)value).Ticks.ToString();
            }
            else if (type == typeof(UnityEngine.Color32))
            {
                return ParseColor(value, num);
            }
            else if (type.Name == "List`1")
            {
                return ParseList(value, num, s);
            }
            else if (type.Name == "Nullable`1")
            {
                if (value == null)
                    return "null";
                return value.ToString();
            }
            else if (type.Name == "Dictionary`2")
            {
                return ParseDictionary(value, num, s);
            }
            else if (type.Name.Split('[').Length > 1 && type.Name.Split('[')[1] == "]")
            {
                return ParseMass(value, num, s);
            }
            else if (type == typeof(bool) || (type.FullName.Split('+').Length > 0 && type.FullName.Split('+')[0] == type.Namespace + ".Enumerators"))
            {
                return (Convert.ToInt32(value)).ToString();
            }
            else// if (type.FullName.Split('.').Length > 0 && type.FullName.Split('.')[0] == type.Namespace)
            {
                return ParseClass(value, num, type, s);
            }
            //else
            //{
            //    UnityEngine.Debug.LogError("PARSER NOT HAVE THIS TYPE!!! : " + value + " " + num + " | " + type.Name + " | " + type.Namespace);
            //    return "";
            //}
        }

        private static string ParseClass<T>(T Value, int num, Type type, string s)
        {
            if (Value == null) return "null" + s;

            string value = "";
            num++;

            var fields = type.GetFields().Where(f => GetNonStaticPublic(f)).ToArray();
            var props = type.GetProperties().Where(f => GetIgnoreProp(f)).ToArray();


            for (int i = 0; i < fields.Length; i++)
            {
                if (i < fields.Length - 1)
                {
                    if (fields[i].FieldType.Name == "Nullable`1")
                    {
                        value += "" + Parse(fields[i].GetValue(Value), fields[i].FieldType, num) + splitChars[num];
                    }
                    else
                    {
                        value += "" + Parse(Convert.ChangeType(fields[i].GetValue(Value), fields[i].FieldType), fields[i].FieldType, num) + splitChars[num];
                    }
                }
                else
                {
                    if (props.Length > 0)
                    {
                        if (fields[i].FieldType.Name == "Nullable`1")
                        {
                            value += "" + Parse(fields[i].GetValue(Value), fields[i].FieldType, num) + splitChars[num];
                        }
                        else
                        {
                            value += "" + Parse(Convert.ChangeType(fields[i].GetValue(Value), fields[i].FieldType), fields[i].FieldType, num) + splitChars[num];
                        }
                    }
                    else
                    {
                        // UnityEngine.Debug.Log();
                        if (fields[i].FieldType.Name == "Nullable`1")
                        {
                            value += "" + Parse(fields[i].GetValue(Value), fields[i].FieldType, num) + splitChars[num];
                        }
                        else
                        {
                            value += "" + Parse(Convert.ChangeType(fields[i].GetValue(Value), fields[i].FieldType), fields[i].FieldType, num);
                        }
                    }
                }
            }

            for (int i = 0; i < props.Length; i++)
            {
                if (i < props.Length - 1)
                {
                    if (props[i].PropertyType.Name == "Nullable`1")
                    {
                        value += "" + Parse(props[i].GetValue(Value, null), props[i].PropertyType, num) + splitChars[num];
                    }
                    else
                    {
                        value += "" + Parse(Convert.ChangeType(props[i].GetValue(Value, null), props[i].PropertyType), props[i].PropertyType, num) + splitChars[num];
                    }
                }
                else
                {
                    if (props[i].PropertyType.Name == "Nullable`1")
                    {
                        value += "" + Parse(props[i].GetValue(Value, null), props[i].PropertyType, num) + splitChars[num];
                    }
                    else
                    {
                        value += "" + Parse(Convert.ChangeType(props[i].GetValue(Value, null), props[i].PropertyType), props[i].PropertyType, num);
                    }
                }
            }

            return value;
        }

        private static string ParseDictionary<T>(T data, int num, string s)
        {
            if (data == null) return "null" + s;

            string value = "";

            var prop = data.GetType().GetProperty("Count");
            int count = (int)prop.GetValue(data, null);
            var prop1 = data.GetType().GetProperty("Item");


            var items = data.GetType().GetProperty("Keys", BindingFlags.Instance | BindingFlags.Public).GetValue(data, null) as IEnumerable;
            if (items == null) return value;
            Type keyType = data.GetType().GetGenericArguments()[0];
            object[] data1 = items.OfType<object>().Select(o => o).ToArray();

            num++;

            for (int i = 0; i < count; i++)
            {
                object obj = GetSProp(data, i, prop1, data1[i], keyType);

                if (obj != null)
                {
                    if (i < count - 1)
                    {
                        value += "" + data1[i] + splitChars[num] + Parse(obj, obj.GetType(), num) + splitChars[num];
                    }
                    else
                    {
                        value += "" + data1[i] + splitChars[num] + Parse(obj, obj.GetType(), num);
                    }
                }
                else
                {
                    if (i < count - 1)
                    {
                        value += "" + Parse(obj, data.GetType(), num) + splitChars[num];
                    }
                    else
                    {
                        if (count == 1)
                        {
                            value += "" + Parse(obj, data.GetType(), num, "Elem");
                        }
                        else
                        {
                            value += "" + Parse(obj, data.GetType(), num);
                        }
                    }
                }
            }

            return value;
        }

        private static object GetSProp<T>(T data, int i, PropertyInfo prop1, object data1, Type keyType)
        {
            //TODO: NEED DECRYPT KEY

            return prop1.GetValue(data, new object[] { Convert.ChangeType(data1, keyType) });
        }

        private static string ParseList<T>(T Value, int num, string s)
        {
            if (Value == null) return "null";

            string value = "";

            var prop = Value.GetType().GetProperty("Count");
            int count = (int)prop.GetValue(Value, null);
            var prop1 = Value.GetType().GetProperty("Item");

            num++;

            for (int i = 0; i < count; i++)
            {
                object obj = prop1.GetValue(Value, new object[] { i });

                if (obj != null)
                {
                    if (i < count - 1)
                    {
                        value += "" + Parse(obj, obj.GetType(), num) + splitChars[num];
                    }
                    else
                    {
                        value += "" + Parse(obj, obj.GetType(), num);
                    }
                }
                else
                {
                    if (i < count - 1)
                    {
                        value += "" + Parse(obj, Value.GetType(), num) + splitChars[num];
                    }
                    else
                    {
                        if (count == 1)
                        {
                            value += "" + Parse(obj, Value.GetType(), num, "Elem");
                        }
                        else
                        {
                            value += "" + Parse(obj, Value.GetType(), num);
                        }
                    }
                }
            }

            return value;
        }

        private static string ParseMass<T>(T Value, int num, string s)
        {
            if (Value == null) return "null" + s;

            string value = "";

            var prop = Value.GetType().GetProperty("Length");
            int count = (int)prop.GetValue(Value, null);
            MethodInfo methodInfo = Value.GetType().GetMethods().Where(m => Filter(m)).First();

            num++;

            for (int i = 0; i < count; i++)
            {
                object obj = methodInfo.Invoke(Value, new object[1] { i });

                if (obj != null)
                {
                    if (i < count - 1)
                    {
                        value += "" + Parse(obj, obj.GetType(), num) + splitChars[num];
                    }
                    else
                    {
                        value += "" + Parse(obj, obj.GetType(), num);
                    }
                }
                else
                {
                    if (i < count - 1)
                    {
                        value += "" + Parse(obj, Value.GetType(), num) + splitChars[num];
                    }
                    else
                    {
                        if (count == 1)
                        {
                            value += "" + Parse(obj, Value.GetType(), num, "Elem");
                        }
                        else
                        {
                            value += "" + Parse(obj, Value.GetType(), num);
                        }
                    }
                }
            }

            return value;
        }

        private static bool Filter(MethodInfo m)
        {
            if (m.Name == "GetValue")
            {
                var parameters = m.GetParameters();
                if (parameters.Count() == 1)
                {
                    var parameter = parameters.First();
                    if (parameter.ParameterType == typeof(int))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static string ParseColor<T>(T color, int num)
        {
            if (color == null)
                return "";

            string value = "";
            num++;

            var field = color.GetType().GetField("r");
            var field1 = color.GetType().GetField("g");
            var field2 = color.GetType().GetField("b");
            var field3 = color.GetType().GetField("a");

            field.GetValue(color);

            value += "" + field.GetValue(color).ToString() + splitChars[num] +
                    field1.GetValue(color).ToString() + splitChars[num] +
                    field2.GetValue(color).ToString() + splitChars[num] +
                    field3.GetValue(color).ToString();

            return value;
        }
        #endregion

        #region Decrypt
        public static T DeParse<T>(string value, int numFirstCharPars = -1)
        {
            return (T)DeParse(value, typeof(T), numFirstCharPars);
        }

        private static object DeParse(string value, Type type, int num)
        {
            
            if (type == typeof(string))
            {
                return DeParseString(value);
            }
            else if (type == typeof(int))
            {
                return DeParseInt(value);
            }
            else if (type == typeof(uint))
            {
                return DeParseUInt(value);
            }
            else if (type == typeof(ulong))
            {
                return DeParseULong(value);
            }
            else if (type == typeof(float))
            {
                return DeParseFloat(value);
            }
            else if (type == typeof(long))
            {
                return DeParseLong(value);
            }
            else if (type == typeof(DateTime))
            {
                return DeParseDateTime(value);
            }
            else if (type == typeof(UnityEngine.Color32))
            {
                return DeParseColor(value, num);
            }
            else if (type == typeof(bool))
            {
                return DeParseBool(value);
            }
            else if (type == typeof(TimeSpan))
            {
                return DeParseTimeSpan(value);
            }
            else if (type.Name == "List`1")
            {
                return DeParseList(value, type, num);
            }
            else if (type.Name == "Dictionary`2")
            {
                return DeParseDictionary(value, type, num);
            }
            else if (type.Name.Split('[').Length > 1 && type.Name.Split('[')[1] == "]")
            {
                Type typeMass;
                if (type.Name.Split('[').Length > 2)
                {
                    typeMass = Type.GetType(type.FullName.Split('[')[0] + "[]");
                }
                else
                {
                    typeMass = Type.GetType(type.FullName.Split('[')[0]);
                }
                return DeParseMass(value, typeMass, num);
            }
            else if ((type.FullName.Split('+').Length > 0 && type.FullName.Split('+')[0] == type.Namespace + ".Enumerators"))
            {
                return DeParseEnum(value, type);
            }
            else if (type.Name == "Nullable`1")
            {
                if (value == "null")
                {
                    return null;
                }
                if (type == typeof(int?))
                {
                    return DeParseInt(value);
                }
                else if (type == typeof(uint?))
                {
                    return DeParseUInt(value);
                }
                else if (type == typeof(ulong?))
                {
                    return DeParseULong(value);
                }
                else if (type == typeof(float?))
                {
                    return DeParseFloat(value);
                }
                else if (type == typeof(long?))
                {
                    return DeParseLong(value);
                }
                else
                {
                    UnityEngine.Debug.LogError("ADD NEW TYPE");
                    return null;
                }
            }
            else //if (type.FullName.Split('.').Length > 0 && type.FullName.Split('.')[0] == type.Namespace)
            {
                return DeParseClass(value, type, num);
            }

            //else
            //{
            //    UnityEngine.Debug.LogError("PARSER DOESN'T HAVE THIS TYPE!!! : " + value + " " + num + " | " + type.Name + " | " + type.Namespace.Split('.').Length + " " + type.FullName.Split('.').Length);
            //    return "";
            //}

        }

        private static object DeParseClass(string value, Type type, int num)
        {
            if (value == "null") return null;
            if (value == "nullElem") { value = "null"; }

            try
            {
                object instClass = Activator.CreateInstance(type);
                num++;

                string[] classString = value.Split(splitChars[num]);



                var fields = type.GetFields().Where(f => GetNonStaticPublic(f)).ToArray();
                var prop = type.GetProperties().Where(f => GetIgnoreProp(f)).ToArray();

                if (value == "") return instClass;

                for (int i = 0; i < fields.Length; i++)
                {
                    if (fields[i].FieldType.Name == "Nullable`1")
                    {
                        fields[i].SetValue(instClass, DeParse(classString[i], fields[i].FieldType, num));
                    }
                    else
                    {
                        fields[i].SetValue(instClass, Convert.ChangeType(DeParse(classString[i], fields[i].FieldType, num), fields[i].FieldType));

                    }
                }

                for (int i = 0; i < prop.Length; i++)
                {
                    if (prop[i].PropertyType.Name == "Nullable`1")
                    {
                        prop[i].SetValue(instClass, DeParse(classString[i + fields.Length], prop[i].PropertyType, num), null);
                    }
                    else
                    {
                        prop[i].SetValue(instClass, Convert.ChangeType(DeParse(classString[i + fields.Length], prop[i].PropertyType, num), prop[i].PropertyType), null);

                    }
                }

                return instClass;
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("ERROR: " + ex);
                throw;
            }


        }

        public static object DeParseDictionary(string value, Type typeDictionary, int num)
        {
            if (value == "null") return null;
            if (value == "nullElem") { value = "null"; }

            num++;
            string[] dictionaryString = value.Split(splitChars[num]);

            Type dictionary = typeof(Dictionary<,>);
            Type[] typeArguments = typeDictionary.GetGenericArguments();
            Type typeKey = typeArguments[0];
            Type type = typeArguments[1];

            Type constructed = dictionary.MakeGenericType(typeArguments);
            IDictionary newDictionary = (IDictionary)Activator.CreateInstance(constructed);

            for (int y = 0; y < dictionaryString.Length; y = y + 2)
            {
                if (value != "")
                {
                    newDictionary.Add(DeParse(dictionaryString[y], typeKey, num), DeParse(dictionaryString[y + 1], type, num));
                }
            }

            return newDictionary;
        }

        public static object DeParseList(string value, Type type, int num)
        {
            if (value == "null") return null;
            if (value == "nullElem") { value = "null"; }

            num++;
            string[] ListString = value.Split(splitChars[num]);

            Type list = typeof(List<>);
            Type[] typeArgs = type.GetGenericArguments();

            Type constructed = list.MakeGenericType(typeArgs);
            IList newList = (IList)Activator.CreateInstance(constructed);

            for (int y = 0; y < ListString.Length; y++)
            {
                if (value != "")
                {
                    newList.Add(DeParse(ListString[y], typeArgs[0], num));
                }
            }

            return newList;
        }


        public static object DeParseMass(string value, Type type, int num)
        {
            if (value == "null") return null;
            if (value == "nullElem") { value = "null"; }
            num++;
            string[] massString = value.Split(splitChars[num]);

            object[] massValue = new object[massString.Length];

            //if (value == "") return ConvertToMass(massValue, type);

            for (int y = 0; y < massString.Length; y++)
            {
                massValue[y] = DeParse(massString[y], type, num);
            }

            return ConvertToMass(massValue, type);
        }

        public static object ConvertToMass(object[] objArr, Type finalType)
        {
            var arr = Array.CreateInstance(finalType, objArr.Length);
            Array.Copy(objArr, arr, objArr.Length);
            return arr;
        }

        private static string DeParseString(string value) {
            if (value == "null")
                return null;
            return value; }
        private static bool DeParseBool(string value) { return Convert.ToBoolean(Convert.ToInt32(value)); }
        private static int DeParseInt(string value) { return Convert.ToInt32(value); }
        private static uint DeParseUInt(string value) { return Convert.ToUInt32(value); }
        private static ulong DeParseULong(string value) { return Convert.ToUInt64(value); }
        private static long DeParseLong(string value) { return Convert.ToInt64(value); }
        private static float DeParseFloat(string value)
        {
            //try
            //{
            //MM.DebugManager.Log(float.Parse(value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) + "");
            //MM.DebugManager.Log(float.Parse(value) + "");

            try
            {
                return float.Parse(value);
            }
            catch
            {
                try
                {
                    return float.Parse(value.Replace(".", ","));
                }
                catch
                {
                    
                    //UnityEngine.Debug.LogError("*********" + value);
                    return float.Parse(value.Replace(",", "."));
                }
            }

            //}
            //catch
            //{
            //    if(value.Contains('.'))
            //    {
            //        value = value.Replace('.', ',');
            //    }
            //    else
            //    {
            //        value = value.Replace(',', '.');
            //    }
            //    return float.Parse(value);
            //}
        }
        private static TimeSpan DeParseTimeSpan(string value) { return TimeSpan.Parse(value); }
        private static DateTime DeParseDateTime(string value)
        {
            //try
            //{
            //    return DateTime.Parse(value, System.Globalization.CultureInfo.CurrentCulture);
            //}
            //catch
            //{
            //    try
            //    {
            //        return DateTime.Parse(value, System.Globalization.CultureInfo.CurrentCulture);
            //    }
            //    catch
            //    {
            //        try
            //        {
            //            return DateTime.Parse(value, System.Globalization.CultureInfo.InvariantCulture);
            //        }
            //        catch
            //        {
            //            MM.DebugManager.LogError("All Bad");
            //        }
            //    }
            //}

            long t = long.Parse(value);
            return new DateTime(t);
            //return DateTime.Parse(value);
        }
        private static UnityEngine.Color32 DeParseColor(string value, int num)
        {
            UnityEngine.Color32 color = new UnityEngine.Color32();
            num++;
            int i = 0;
            string[] colorString = value.Split(splitChars[num]);
            color.r = Convert.ToByte(colorString[i]); i++;
            color.g = Convert.ToByte(colorString[i]); i++;
            color.b = Convert.ToByte(colorString[i]); i++;
            color.a = Convert.ToByte(colorString[i]); i++;
            return color;
        }
        private static object DeParseEnum(string value, Type type)
        {
            return Convert.ChangeType(Enum.Parse(type, value), type);
        }
        #endregion

        private static bool GetNonStaticPublic(FieldInfo f)
        {
            if (!f.IsStatic && f.IsPublic)
            {
                return true;
            }
            else return false;
        }

        private static bool GetIgnoreField(FieldInfo f)
        {
            for (int i = 0; i < f.GetCustomAttributes(false).Length; i++)
            {
                if (f.GetCustomAttributes(false)[i].GetType() == typeof(ParserIgnoreAttribute))
                {
                    return false;
                }
            }

            return true;
        }

        private static bool GetIgnoreProp(PropertyInfo f)
        {
            for (int i = 0; i < f.GetCustomAttributes(false).Length; i++)
            {
                if (f.GetCustomAttributes(false)[i].GetType() == typeof(ParserIgnoreAttribute))
                {
                    return false;
                }
            }

            return true;
        }

        public static T FindeType<T>(string value, int n, int numFindeChar = 1)
        {
            if (n >= 0)
            {
                string[] s = value.Split(splitChars[numFindeChar]);

                try
                {
                    return (T)DeParse(s[n], typeof(T), 0);

                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.Log(value + "| NNNNNNNNNNNNN " + ex.Message + "PARSER!!!");
                    throw;
                }
            }
            else
            {
                string[] s = value.Split(splitChars[numFindeChar]);
                try
                {
                    return (T)DeParse(s[s.Length + n], typeof(T), 0);
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.Log(value + "| NNNNNNNNNNNNN " + ex.Message + "PARSER!!!");
                    throw;
                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ParserIgnoreAttribute : Attribute
    {
    }
