﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Runtime.InteropServices;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif


public class GeofancingManager : MonoBehaviour
{
    [SerializeField] private Text _outputTextField = null;

    private List<Action> _mainThreadCallbacks = new List<Action>();

    private AndroidJavaObject geofencing;


#if UNITY_IOS 

    [DllImport("__Internal")]
    private static extern void AddRegion(float latitude, float longitude, float radius, string name);

    [DllImport("__Internal")]
    private static extern void RunIOSGeofencing();

    [DllImport("__Internal")]
    private static extern void RemoveRegion(string name);

#endif

    void Start()
    {
#if UNITY_ANDROID

        geofencing = new AndroidJavaClass("com.example.geofencinglib.Geofencing");

        string javaCallbacksInterface = "com.example.geofencinglib.CSharpCallbacks";

        JavaStaticBridge callbackBridge = new JavaStaticBridge(javaCallbacksInterface, EventHandler);

        geofencing.CallStatic("SetCallbacks", callbackBridge);

        //AndroidJavaClass unityClass = new AndroidJavaClass("com.example.customunityplayeractivity.UnityPlayer");
        AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");

        geofencing.CallStatic("setContext", unityActivity);

#elif UNITY_IOS
       
        ObjectiveCStaticBridge.AddUnityCallbackHandler(EventHandler);
        ObjectiveCStaticBridge.RegisterCallbackInObjectiveC();

#endif

        InitializeSavedZones();
    }

    private void InitializeSavedZones()
    {
        List<GeofenceZone> zones = DataStorage.GetItems();

        if (zones.Count > 0)
        {
            foreach (GeofenceZone z in zones)
            {
                AddGeofence(z);
            }
        }
    }

    private void Update()
    {
        if (_mainThreadCallbacks.Count > 0)
        {
            for (int i = 0; i < _mainThreadCallbacks.Count; i++)
            {
                _mainThreadCallbacks[i].Invoke();
            }

            _mainThreadCallbacks.Clear();
        }
    }

    private void EventHandler(string message)
    {
        Action handler = () =>
        {
            string delimiter = "---------";
            string updatedText = $"{_outputTextField.text} \n {message} \n {delimiter}";

            _outputTextField.text = updatedText;
        };

        _mainThreadCallbacks.Add(handler);
    }

    public void RunGeofencing()
    {
#if UNITY_ANDROID
        geofencing.CallStatic("Start");
#elif UNITY_IOS
        RunIOSGeofencing();
#endif
    }

    public void AddGeofence(GeofenceZone zone)
    {
        float latitude = float.Parse(zone.latitude);
        float longitude = float.Parse(zone.longitude);
        float radius = float.Parse(zone.radius);

#if UNITY_ANDROID
        geofencing.CallStatic("addGeofence", latitude, longitude, radius, zone.id);
#elif UNITY_IOS
        AddRegion(latitude, longitude, radius, zone.id);
#endif
    }

    public void RemoveGeofence(string id)
    {
#if UNITY_ANDROID
        geofencing.CallStatic("removeGeofence", id);
#elif UNITY_IOS
        RemoveRegion(id);
#endif
    }
}
