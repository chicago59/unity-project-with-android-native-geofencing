﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GeofenceZone
{
    public string longitude;
    public string latitude;
    public string radius;
    public string id;

    private static char _valueDelimiter = ':'; 
    private static char _fieldDelimiter = ';';
    private static char _zoneDelimiter = '$';

    public static string ConvertToString(List<GeofenceZone> list)
    {
        string result = "";

        foreach (var item in list)
        {
            result += ConvertToString(item) + _zoneDelimiter;
        }

        return result;
    }

    public static string ConvertToString(GeofenceZone zone)
    {
        string result = "";

        result += $"id{_valueDelimiter}{zone.id}{_fieldDelimiter}";
        result += $"radius{_valueDelimiter}{zone.radius}{_fieldDelimiter}";
        result += $"latitude{_valueDelimiter}{zone.latitude}{_fieldDelimiter}";
        result += $"longitude{_valueDelimiter}{zone.longitude}{_fieldDelimiter}";

        return result;
    }

    public static List<GeofenceZone> ConvertFromString(string source)
    {
        List<GeofenceZone> result = new List<GeofenceZone>();
        string[] zones = source.Split(_zoneDelimiter);

        Debug.Log($"source: {source}");
        Debug.Log($"zones lenght: {zones.Length}");

        foreach (var zone in zones)
        {
            Debug.Log($"zone: {zone}");
            Debug.Log($"zone length: {zone.Length}");
            string[] fields = zone.Split(_fieldDelimiter);
            GeofenceZone zoneObj = new GeofenceZone();

            Debug.Log($"fields lengtth: {fields.Length}");

            for (int i = 0; i < fields.Length; i++)
            {
                Debug.Log($"value split: {fields[i].Split(_valueDelimiter)[0]} {fields[i].Split(_valueDelimiter)[1]}");
                string value = fields[i].Split(_valueDelimiter)[1];
                switch(i)
                {
                    case 0:
                        zoneObj.id = value;
                        break;
                    case 1:
                        zoneObj.radius = value;
                        break;
                    case 2:
                        zoneObj.latitude = value;
                        break;
                    case 3:
                        zoneObj.longitude = value;
                        break;
                }
            }
        }

        return result;
    }
}
