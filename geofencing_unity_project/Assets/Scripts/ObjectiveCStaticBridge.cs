﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public static class ObjectiveCStaticBridge
{
    private static List<Action<string>> _unityHandlers = new List<Action<string>>();

    private delegate void MonoPMessageDelegate(string message);

#if UNITY_IOS && !UNITY_EDITOR
    
    [DllImport("__Internal")]
    private static extern void RegisterCallbackHandler(MonoPMessageDelegate messageDelegate); 
    
#endif

    [AOT.MonoPInvokeCallback(typeof(MonoPMessageDelegate))]
    private static void OnMessage(string message)
    {
        foreach (var handler in _unityHandlers)
        {
            handler.Invoke(message);
        }
    }

    public static void RegisterCallbackInObjectiveC()
    {
#if UNITY_IOS && !UNITY_EDITOR

        RegisterCallbackHandler(OnMessage);

#endif
    }

    public static void AddUnityCallbackHandler(Action<string> handler)
    {
        _unityHandlers.Add(handler);
    }
}
