﻿using UnityEngine;
using UnityEngine.UI;

public class ZoneListItem : MonoBehaviour
{
    [SerializeField] private Text _idField = null;
    [SerializeField] private Text _latitudeField = null;
    [SerializeField] private Text _longitudeField = null;
    [SerializeField] private Text _radiusField = null;
    [SerializeField] private Button _removeButton = null;

    private GeofancingManager _manager = null;

    private void Awake()
    {
        _removeButton.onClick.AddListener(RemoveItem);
    }

    public void SetValues(string id, string lat, string lon, string radius)
    {
        _idField.text = id;
        _latitudeField.text = lat;
        _longitudeField.text = lon;
        _radiusField.text = radius;
    }

    public void SetGeofencingManager(GeofancingManager manager)
    {
        _manager = manager;
    }

    public void RemoveItem()
    {
        string id = _idField.text;

        DataStorage.RemoveItem(id);
        _manager.RemoveGeofence(id);
    }
}
