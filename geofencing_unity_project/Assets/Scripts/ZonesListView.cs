﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZonesListView : MonoBehaviour
{
    [SerializeField] private GameObject _itemPrefab = null;
    [SerializeField] private ScrollRect _view = null;
    [SerializeField] private GeofancingManager _geofencing = null;

    private void Awake()
    {
        DataStorage.SubscribeOnChangeEvent(DisplayItems);
    }

    public void DisplayItems(List<GeofenceZone> items)
    {
        ClearView();

        foreach (var item in items)
        {
            GameObject itemView = Instantiate(_itemPrefab, _view.content.transform, false);
            ZoneListItem component = itemView.GetComponent<ZoneListItem>();

            component.SetValues(item.id, item.latitude, item.longitude, item.radius);
            component.SetGeofencingManager(_geofencing);
        }
    }

    public void ClearView()
    {
        foreach (Transform child in _view.content.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
