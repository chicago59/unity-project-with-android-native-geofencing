﻿using System;
using UnityEngine;

public class JavaStaticBridge : AndroidJavaProxy
{
    private Action<string> _callbackEvent;

    public JavaStaticBridge(string javaInterface, Action<string> callbackEvent) : base(javaInterface)
    {
        _callbackEvent = callbackEvent;
    }

    public void SendMessageByCallbackToCSharp(string message)
    {
        if (_callbackEvent != null)
        {
            _callbackEvent.Invoke(message);
        }
    }
}
