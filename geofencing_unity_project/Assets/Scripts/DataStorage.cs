﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DataStorage : MonoBehaviour
{
    [SerializeField] private ZonesListView _listView = null;

    private static List<GeofenceZone> _items = new List<GeofenceZone>();
    private static List<Action<List<GeofenceZone>>> _itemsChangedEventCallbacks = new List<Action<List<GeofenceZone>>>();


    private void Awake()
    {
        _items = DataStorageIO.Read();
        _listView.DisplayItems(_items);
    }

    public static void RemoveItem(string id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i].id == id)
            {
                _items.RemoveAt(i);
                DataStorageIO.Write(_items);
                FireOnChangeCallbacks();
                return;
            }
        }
    }

    public static void AddItem(GeofenceZone item)
    {
        _items.Add(item);
        DataStorageIO.Write(_items);
        FireOnChangeCallbacks();
    }

    public static void FireOnChangeCallbacks()
    {
        foreach (var callback in _itemsChangedEventCallbacks)
        {
            callback.Invoke(_items);
        }
    }

    public static void SubscribeOnChangeEvent(Action<List<GeofenceZone>> callback)
    {
        _itemsChangedEventCallbacks.Add(callback);
    }

    public static List<GeofenceZone> GetItems()
    {
        return _items;
    }
}
