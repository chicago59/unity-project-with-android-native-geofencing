﻿using System.IO;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

public class PostprocessBuildConfig : IPostprocessBuildWithReport
{
    public int callbackOrder => 999;

    public void OnPostprocessBuild(BuildReport report)
    {
        BuildTarget buildTarget = report.summary.platform;
        string pathToBuiltProject = report.summary.outputPath;

        ChangeXcodePlist(buildTarget, pathToBuiltProject);
    }

    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // location keys
            rootDict.SetString("NSLocationWhenInUseUsageDescription", "For geofencing");
            rootDict.SetString("NSLocationAlwaysAndWhenInUseUsageDescription", "For geofencing");

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}
