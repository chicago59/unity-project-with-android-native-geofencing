﻿using System.Collections.Generic;
using UnityEngine;

public class DataStorageIO : MonoBehaviour
{
    private const string KEY = "zones";

    public static List<GeofenceZone> Read()
    {
        string data = PlayerPrefs.GetString(KEY);
        List<GeofenceZone> readedData = Parser.DeParse<List<GeofenceZone>>(data, 0);

        return readedData != null ? readedData : new List<GeofenceZone>();
    }

    public static void Write(List<GeofenceZone> items)
    {
        string data = Parser.Parse(items, 0);
        PlayerPrefs.SetString(KEY, data);
    }
}
