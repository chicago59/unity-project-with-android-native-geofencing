﻿using UnityEngine;
using UnityEngine.UI;

public class GeofenceForm : MonoBehaviour
{
    [SerializeField] private InputField _latitudeField = null;
    [SerializeField] private InputField _longitudeField = null;
    [SerializeField] private InputField _radiusField = null;
    [SerializeField] private InputField _idField = null;
    [SerializeField] private GeofancingManager _geofencingManager = null;


    private string _longitude;
    private string _latitude;
    private string _radius;
    private string _id;


    public void Clear()
    {
        _latitudeField.text = "";
        _longitudeField.text = "";
        _radiusField.text = "";
        _idField.text = "";
    }

    public void LatitudeFieldHandler(string value)
    {
        _latitude = value;
    } 

    public void LongitudeFieldHandler(string value)
    {
        _longitude = value;
    }

    public void RadiusFieldHandler(string value)
    {
        _radius = value;
    }

    public void IdFieldHandler(string value)
    {
        _id = value;
    }

    public void Submit()
    {
        GeofenceZone zone = new GeofenceZone() { id = _id, latitude = _latitude, longitude = _longitude, radius = _radius};
        DataStorage.AddItem(zone);
        _geofencingManager.AddGeofence(zone);
        Clear();
    }

    public string GetLatitude()
    {
        return _latitude;
    }

    public string GetLongitude()
    {
        return _longitude;
    }

    public string GetRadius()
    {
        return _radius;
    }

    public string GetId()
    {
        return _id;
    }
}
