package com.example.geofencinglib;

public interface CSharpCallbacks {

    void SendMessageByCallbackToCSharp(String message);

}
