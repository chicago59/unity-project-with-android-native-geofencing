package com.example.geofencinglib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


import com.google.android.gms.location.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class GeofenceBroadcastReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceStatusCodes
                    .getStatusCodeString(geofencingEvent.getErrorCode());

            Geofencing.FireCallback("Geofencing event has error");
            return;
        }

        Geofencing.FireCallback("Geofence transition occurs");

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        DisplayEventInfo(geofencingEvent);

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();


//            double lat = geofencingEvent.getTriggeringLocation().getLatitude();
//            double lon = geofencingEvent.getTriggeringLocation().getLongitude();

            String[] zoneIds = getGeofencesIds(geofencingEvent);
            String zoneIdsStr = "";

            for (int i = 0; i < zoneIds.length; i++) {
                zoneIdsStr += zoneIds[i] + ", ";
            }

            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
//                Geofencing.FireCallback("Geofence transition trigger type GEOFENCE_TRANSITION_ENTER was occured at location: lat: " + lat + " lon: " + lon);

                Geofencing.FireNotification(context, "Geofence transition type", "GEOFENCE_TRANSITION_ENTER. Zone ids: " + zoneIdsStr);
            }

            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
//                Geofencing.FireCallback("Geofence transition trigger type GEOFENCE_TRANSITION_EXIT was occured at location: lat: " + lat + " lon: " + lon);

                Geofencing.FireNotification(context, "Geofence transition type", "GEOFENCE_TRANSITION_EXIT Zone ids: " + zoneIdsStr);
            }

            // Send notification and log the transition details.
            ShowToast(context, "geofenceTransition success " + geofenceTransition);
        } else {
            // Log the error.
            ShowToast(context, "geofence transtion error");
        }
    }

    private void DisplayEventInfo(GeofencingEvent e) {
        int transition = e.getGeofenceTransition();

        if (transition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                transition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            List<Geofence> triggeringGeofences = e.getTriggeringGeofences();


            Geofence[] triggers = new Geofence[triggeringGeofences.size()];
            triggeringGeofences.toArray(triggers);

            for (int i = 0; i < triggers.length; i++) {
                String triggerType = "";
                String geofenceId = triggers[i].getRequestId();

                if (transition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                    triggerType = "GEOFENCE_TRANSITION_ENTER";
                }

                if (transition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                    triggerType = "GEOFENCE_TRANSITION_EXIT";
                }

                double lat = e.getTriggeringLocation().getLatitude();
                double lon = e.getTriggeringLocation().getLongitude();

                Geofencing.FireCallback("Geofence transition trigger type: " + triggerType);
                Geofencing.FireCallback("Was occur in geofence with id: " + geofenceId);
                Geofencing.FireCallback("At coords: latitude: " + lat + " longitude: " + lon);
            }
        } else {
            Geofencing.FireCallback("An unexpected trigger has worked");
        }
    }

    private String[] getGeofencesIds(GeofencingEvent e) {
        List<Geofence> triggeringGeofences = e.getTriggeringGeofences();

        Geofence[] triggers = new Geofence[triggeringGeofences.size()];
        triggeringGeofences.toArray(triggers);

        String[] ids = new String[triggers.length];

        for (int i = 0; i < triggers.length; i++) {
            ids[i] = triggers[i].getRequestId();
        }

        return ids;
    }

    private void ShowToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}