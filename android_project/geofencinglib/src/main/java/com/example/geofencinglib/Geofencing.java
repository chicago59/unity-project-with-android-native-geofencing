package com.example.geofencinglib;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;


public class Geofencing {
    private static GeofencingClient geofencingClient;
    private static PendingIntent geofencePendingIntent;

    private static final String NOTIFICATION_CHANNEL_ID = "GEOFENCING_NOTIFICATION";

    private static Context currentActivityContext;

    private static int notificationIdCounter = 0;

    private static CSharpCallbacks callbacks;
    private static ArrayList<Geofence> geofences = new ArrayList<Geofence>();

    public static void setContext(Context context) {
        currentActivityContext = context;
        createNotificationChannel();
    }

    private static void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "geofencing_notification_channel";
            String description = "geofencing_notification_channel_desc";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = currentActivityContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    public static void SetCallbacks(CSharpCallbacks SharpCallbackObject) {
        callbacks = SharpCallbackObject;
    }

    public static void FireCallback(String message) {
        if (callbacks != null) {
            callbacks.SendMessageByCallbackToCSharp(message);
        }
    }

    public static void addGeofence(float lat, float lon, float radius, String id) {
        Geofence newPoint = new Geofence.Builder()
                .setRequestId(id)
                .setCircularRegion(
                        lat,
                        lon,
                        radius
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();

        geofences.add(newPoint);

        Geofencing.FireCallback("Geofence was added to the list");
    }

    public static List<Geofence> getGeofences() {
        return geofences;
    }

    public static void Start(){
        Geofencing.FireNotification(currentActivityContext, "Geofencing", "Starting geofencing service");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            currentActivityContext.startForegroundService(new Intent(currentActivityContext, GeofencingService.class));
        } else {
            geofencingClient = LocationServices.getGeofencingClient(currentActivityContext);

            geofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Geofencing.FireCallback("Geofencing successfully started");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(currentActivityContext, "Error occurs when adding geofences " + e, Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    public static void removeGeofence(final String id) {
        ArrayList<String> idsList = new ArrayList<String>();
        idsList.add(id);

        geofencingClient.removeGeofences(idsList)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Geofencing.FireCallback("Geofencing with id: " + id + " successfully removed");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(currentActivityContext, "Geofence removing failed " + e, Toast.LENGTH_LONG).show();
                    }
                });
    }

//    public static void Stop(Context context) {
//        context.stopService(new Intent(context, GeofencingService.class));
//    }

    private static GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
//        builder.addGeofence(getTestGeofence());
        builder.addGeofences(Geofencing.getGeofences());
        return builder.build();
    }

    private Geofence getTestGeofence() {
        return new Geofence.Builder()
                .setRequestId("1")
                .setCircularRegion(
                        51.522360,
                        31.278269,
                        100
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    private static PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }
        Intent intent = new Intent(currentActivityContext, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        geofencePendingIntent = PendingIntent.getBroadcast(currentActivityContext, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }

    public static void FireNotification(Context context, String title, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(notificationIdCounter, builder.build());
        notificationIdCounter++;
    }

}
